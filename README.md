# README #

Google geocoder

### Example ###

```php
<?php
$api_key = 'YOUR_API_KEY';
$latitude = 51.509865;
$longitude = -0.118092;
$Geocoder = new Geocoder($api_key);
/*
array (size=3)
  0 => string 'London' (length=6)
  1 => string 'United Kingdom' (length=14)
  2 => string 'GB' (length=2)
 */
var_dump($Geocoder->reverse($latitude, $longitude));
```
