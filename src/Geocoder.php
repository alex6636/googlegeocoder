<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   2017
 *
 * Google geocoder
 */

namespace alexs\google\geocoder;
use RuntimeException;

class Geocoder
{
    public
        $api_key,
        $connection_timeout = 10; // sec

    public function __construct($api_key) {
        $this->api_key = $api_key;
    }

    /**
     * @param double $latitude
     * @param double $longitude
     * @throws RuntimeException
     * @return mixed
     * false or string if has errors or invalid data
     * or array ['city', 'country', 'country_iso']
     */
    public function reverse($latitude, $longitude) {
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $latitude . ',' . $longitude .
               '&key=' . $this->api_key;
        $contents = $this->getContents($url);
        if ($contents === false) {
            throw new RuntimeException('An error occurred while getting the content');
        }
        $arr = json_decode($contents, true);
        if (isset($arr['error_message'])) {
            throw new RuntimeException($arr['error_message']);
        }
        return $this->parseResults($arr['results']);
    }

    protected function parseResults($results) {
        $result = [];
        if (!empty($results)) {
            foreach ($results as $rs) {
                if (!empty($rs['address_components'])) {
                    foreach ($rs['address_components'] as $component) {
                        foreach ($component as $comp_parts) {
                            foreach ($component['types'] as $type) {
                                if ($type === 'locality') {
                                    $result[0] = $component['long_name'];
                                }
                                if ($type === 'country') {
                                    $result[1] = $component['long_name'];
                                    $result[2] = $component['short_name'];
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
        return $result;
    }

    protected function getContents($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connection_timeout);
        $contents = curl_exec($ch);
        curl_close($ch);
        return $contents;
    }
}