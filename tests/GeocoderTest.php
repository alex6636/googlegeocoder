<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   08-Sep-17
 */

namespace alexs\google\geocoder\tests;
use alexs\google\geocoder\Geocoder;
use PHPUnit\Framework\TestCase;

class GeocoderTest extends TestCase
{
    /**
     * @expectedException \RuntimeException
     */
    public function testInvalidApiKey() {
        $api_key = 'INVALIDKEY';
        $latitude = 51.509865;
        $longitude = -0.118092;
        $Geocoder = new Geocoder($api_key);
        $Geocoder->reverse($latitude, $longitude);
    }

    public function testReverse() {
        $api_key = 'AIzaSyD7TH4WHO1neEyo5sxw86WesqMI4QP5eTg';
        $latitude = 51.509865;
        $longitude = -0.118092;
        $Geocoder = new Geocoder($api_key);
        $result = $Geocoder->reverse($latitude, $longitude);
        $this->assertEquals($result, ['London', 'United Kingdom', 'GB']);
    }
}